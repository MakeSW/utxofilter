const fs = require('fs');

const lineReader = require('line-reader');

const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')

function minFactory(attr, minValue) {
    return ((utxo) => {
        if (!!utxo && typeof utxo[attr] !== 'undefined') {
            return minValue <= utxo[attr];
        }

        return false;
    });
}   

function maxFactory(attr, maxValue) {
    return ((utxo) => {
        if (!!utxo && typeof utxo[attr] !== 'undefined') {
            return maxValue >= utxo[attr];
        }

        return false;
    });
}

function printFilteredColumns(utxoLine, columns) {
    const result = [];

    (columns || []).forEach(columnName => {
        result.push(utxoLine[columnName] || '');
    });
    
    if (result.length > 0) {
        console.log(result.join(','));
    }
}

function getTypeFilter(allowedTypes) {
    return (utxo) => {
        return allowedTypes.some(t => !!utxo && typeof utxo.type === 'string' && t.toUpperCase() === utxo.type.toUpperCase());
    }
}

const filterFactories = {
    maxHeight : (max) => maxFactory('height', max),
    minHeight: (min) => minFactory('height', min),
    maxAmount: (max) => maxFactory('amount', max),
    minAmount: (min) => minFactory('amount', min),
    type : (options) => getTypeFilter(options),
};
const paramParser = {
    height : (val) => parseInt(val),
    amount : (val) => parseInt(val)
};

const argv = yargs(hideBin(process.argv))
    .option('maxHeight', {
        type: 'number',
        describe: 'max value of block height of the UTXO lines you are interested in'
    })
    .option('minHeight', {
        type: 'number',
        describe: 'min value of block height of the UTXO lines you are intereted in'
    })
    .option('minAmount', {
        type: 'number',
        describe: 'min amount of satoshis on address of UTXO lines you are interested in'
    })
    .option('maxAmount', {
        type: 'number',
        describe: 'max amount of satoshis on address of UTXO lines you are interested in'
    })
    .option('format', {
        alias: 'f',
        type: 'array',
        describe: 'what columns do you want to reuse from the input file?'
    })
    .option('input', {
        alias: 'i',
        describe: 'path to UTXO CSV file'
    })
    .option('type', {
        alias: 't',
        describe: 'what types of transaction you are interested in',
        type: 'array'
    })
    .option('omitHeader', {
        describe: 'if set, omits the first line containing the column names in the result',
        type: 'boolean'
    })
    .demandOption([ 'input' ], 'UTXO input file is required')
    .help()
    .argv;

const filters = [ ];

Object.keys(filterFactories).forEach(paramName => {
    const paramValue = argv[paramName];

    if (!!paramValue) {
        const getFilterFn = filterFactories[paramName];
        filters.push(getFilterFn(paramValue))
    }
});

if (!fs.existsSync(argv.input)) {
    console.error(`File ${argv.input} does not exist!`);
    return;
}

let isFirstRun = true;
let params = null;

const print = !!argv.format ?
    (utxo, _) => printFilteredColumns(utxo, argv.format) :
    (_, line) => console.log(line);

lineReader.eachLine(argv.input, function(line, last) {
    if (isFirstRun) {
        if (!argv.omitHeader) {
            if (!!argv.format) {
                console.log(argv.format.join(','));
            } else {
                console.log(line);
            }
        }

        params = line.split(',');
        isFirstRun = false;
    } else {
        const values = line.split(',');
        const obj = {};
        params.forEach((param, idx, _) => {
            const map = !!paramParser[param] ? paramParser[param] : (val) => val;
            obj[param] = map(values[idx]);
        });

        if (filters.length === 0 || filters.every(filter => filter(obj))) {
            print(obj, line);
        }
    }
});
